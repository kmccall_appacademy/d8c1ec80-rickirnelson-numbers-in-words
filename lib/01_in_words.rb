class Fixnum
  def in_words
    numarr = self.to_s.split('').reverse.each_slice(3).to_a.map(&:reverse).reverse.map(&:join).map(&:to_i)
    numarr.map! do |n|
      if n < 100
        under_100(n)
      elsif n > 99 && n < 1000
        hundreds(n)
      end
    end
    res = ""
    if numarr.length == 1
      res = numarr[0]
    elsif numarr.length == 2
      res = numarr.join(' thousand ').gsub("zero","").strip
    elsif numarr.length == 3
      mil = ['million','thousand']
      res << numarr.zip(mil).map(&:compact).reject {|a| a.include?("zero")}.join(' ')
    elsif numarr.length == 4
      bil = ['billion','million','thousand']
      res << numarr.zip(bil).map(&:compact).reject {|a| a.include?("zero")}.join(' ')
    elsif numarr.length == 5
      tril = ['trillion','billion','million','thousand']
      res << numarr.zip(tril).map(&:compact).reject {|a| a.include?("zero")}.join(' ')
    end
  end

  def numwords(n)
      numword = {0=>"zero",
                  1=>"one",
                  2=>"two",
                  3=>"three",
                  4=>"four",
                  5=>"five",
                  6=>"six",
                  7=>"seven",
                  8=>"eight",
                  9=>"nine",
                  10=>"ten",
                  11=>"eleven",
                  12=>"twelve",
                  13=>"thirteen",
                  14=>"fourteen",
                  15=>"fifteen",
                  16=>"sixteen",
                  17=>"seventeen",
                  18=>"eighteen",
                  19=>"nineteen",
                  20=>"twenty",
                  30=>"thirty",
                  40=>"forty",
                  50=>"fifty",
                  60=>"sixty",
                  70=>"seventy",
                  80=>"eighty",
                  90=>"ninety"}
      numword[n]
  end

  def under_100(num)
      if num < 21 || num % 10 == 0
        return numwords(num)
      elsif num % 10 != 0 && num.between?(21,99)
        first_dig = num.to_s[0] + "0"
        second_dig = num.to_s[1]
        return "#{numwords(first_dig.to_i)} #{numwords(second_dig.to_i)}"
      end
  end

  def hundreds(num)
      if num > 99 && num < 1000
        first_d = num.to_s[0].to_i
        second_d = (num.to_s[1]+"0").to_i
        third_d = num.to_s[2].to_i
        return "#{numwords(first_d)} hundred" if second_d == 0 && third_d == 0
        return "#{numwords(first_d)} hundred #{numwords(third_d)}" if second_d == 0
        return "#{numwords(first_d)} hundred #{numwords(second_d + third_d)}" if second_d == 10
        return "#{numwords(first_d)} hundred #{numwords(second_d)}" if third_d == 0
        return "#{numwords(second_d)} #{numwords(third_d)}" if first_d == 0
        return "#{numwords(first_d)} hundred #{numwords(second_d)} #{numwords(third_d)}"
      end
  end
end
